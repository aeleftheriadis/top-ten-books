import React, { Component } from 'react';
import './PaneContent.css';
import ResultsList from './ResultsList';
import fetchJsonp from 'fetch-jsonp';

const URL = 'https://itunes.apple.com/search'

class PaneContent extends Component{
    
    constructor(props){
        super(props);
         this.state = {
             author:props.author,
             results:[]
        };
    }
    static propTypes = {
        author: React.PropTypes.string,
        results: React.PropTypes.array
    }
    static defaultProps = {
        author: "",
        results:[]
    }
    shouldComponentUpdate(nextProps, nextState) {
  	    return this.props !== nextProps || this.state !== nextState;
    }
    componentDidMount() {
        this.fetchData();
    }
    _contructUrl(URL,term){        
        term = term.toLowerCase();
        return `${URL}?country=gb&term=${term}&media=ebook&limit=10`
    }
    async fetchData() {
        let term = this.state.author.toLowerCase();      
        const response = await fetchJsonp(this._contructUrl(URL,term))
        const json = await response.json()
        const results = json.results;        
        this.setState({results})
    }
    render(){
        return(
            <ResultsList results={this.state.results} />
        )
    }
}

export default PaneContent;