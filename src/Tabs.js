import React, { Component } from 'react';
import './Tabs.css';

class Tabs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: props.selected
        };
    }  
    static propTypes = {      		      
        select: React.PropTypes.number,      
    }
    static defaultProps = {
        selected: 0
    }
    handleClick(index, event) {
        event.preventDefault();
        this.setState({
            selected: index
        });
    }
    shouldComponentUpdate(nextProps, nextState) {
  	    return this.props !== nextProps || this.state !== nextState;
    }
    _renderTitles() {
        function labels(child, index) {
            let activeClass = (this.state.selected === index ? 'active' : '');
            return (
                <li key={index}>
                    <a href="#" 
                        className={activeClass}
                        onClick={this.handleClick.bind(this, index)}>
                        {child.props.label}
                    </a>
                </li>
            );
        }
        return (
            <ul className="Tabs-labels">
                {this.props.children.map(labels.bind(this))}
            </ul>
        );
    }
    _renderContent() {
        return (
            <div className="Tabs-content">
                {this.props.children[this.state.selected]}
            </div>
        );
    }
    render() {
        return (
            <div className="Tabs">
                {this._renderTitles()}
                {this._renderContent()}
            </div>
        );
    }
}

export default Tabs;