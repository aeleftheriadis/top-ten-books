// @flow
import React, { Component } from 'react';
import './Pane.css';

class Pane extends Component {
    static propTypes = {
        label: React.PropTypes.string.isRequired,
        children: React.PropTypes.element.isRequired
    }
    render() {
        return ( <div className="Pane-container">{ this.props.children } </div>
        );
    }
};

export default Pane;