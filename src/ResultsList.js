import React, { Component } from 'react';
import './ResultsList.css';

class ResultsList extends Component{
    constructor(props){
        super(props);
        this.state = {
            results:props.results
        }
    }
    static propTypes = {
        results: React.PropTypes.array
    }
    static defaultProps = {
        results: []
    }
    shouldComponentUpdate(nextProps, nextState) {
  	    return this.props !== nextProps || this.state !== nextState;
    }
    render(){
        if(!this.props.results){
            return (
                <div className="ResultsList-header"><p>Loading...</p></div>
            );
        }
        if (this.props.results.length === 0) {
            return (
                <div className="ResultsList-header">
                    <p>No results are here... yet.</p>
                </div>
            );
        }
        return(
            <div>
                {
                    this.props.results.map(result => {
                    return (
                            <div className="ResultsList-item" key={result.trackId}>
                                <p className="ResultsList-item-title">{result.trackName}</p>
                                <div className="ResultsList-price"><p>Price: {result.formattedPrice}</p></div>
                                <div className="ResultsList-description" dangerouslySetInnerHTML={{__html: result.description}}></div>                                
                                <div className="ResultList-button-container"><a className="ResultList-button" href={result.trackViewUrl} target="_blank" >More</a></div>
                            </div>
                        );
                    })
                }
            </div>
        )
    }
}

export default ResultsList;
