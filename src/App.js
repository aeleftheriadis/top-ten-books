// @flow
import React, { Component } from 'react';
import './App.css';
import Tabs from './Tabs'
import Pane1 from './Pane1'
import Pane2 from './Pane2'
import Pane3 from './Pane3'
import PaneContent from './PaneContent'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Welcome to Top Ten Books</h2>
          <p className="App-intro">
            Top Ten Books from greatest authors
          </p>    
        </div>      	
        <div>
          <Tabs selected={0}>
            <Pane1 label="Ernest Hemingway">
              <PaneContent author="Hemingway"/>
            </Pane1>
            <Pane2 label="Charles Dickens">
              <PaneContent author="Dickens"/>
            </Pane2>
            <Pane3 label="William Shakespeare">
              <PaneContent author="Shakespeare"/>
            </Pane3>
          </Tabs>
        </div>
      </div>
    );
  }
}

export default App;
